# Wikifunction CI Helm Chart

This Helm chart deploys the WikiLambda CI environment on Kubernetes for testing
and development purposes. The environment includes a MediaWiki instance with the
WikiLambda extension, the services for Wikifunction, agnostic ingress routing

## Installing locally on minikube
### Prerequisites

- [Helm](https://helm.sh/docs/intro/install/) installed
- [kubectl](https://kubernetes.io/docs/tasks/tools/install-kubectl/) installed
- [k9s](https://k9scli.io) installed (optional, for monitoring the deployment)

### Running with k3s

K3s is a small kubernetes distribution. It differs from minikube in that it
turns your local (Linux) machine to a single node kubernetes "cluster". The
control-plane are run as services via systemd rather than as containers in
Docker. The result is, k3s gives you a simple production-like kubernetes
cluster.

1. Install [k3s](https://k3s.io)

1. Get your machine's private IP. This can be found with commands like `ip addr`.

1. Set the `mediawikiUrl` in `values.yaml` using the Minikube IP and sslip.io:

   ```
   mediawikiUrl: <your-private-ip>.sslip.io
   ```

1. (Optional) add the AppArmor profile to limit syscalls the function evaluator
   can make. This requires you to have AppArmor installed. You can skip this step,
   but make sure `functionEvaluator.enableAppArmor` is set to false.

   ```
   sudo cp k8s-wikifunctions-evaluator k8s-wikifunctions-orchestrator /etc/apparmor.d
   sudo systemctl restart apparmor.service
   ```

1. Deploy the WikiLambda CI environment using Helm:

   ```
   helm install wikilambda . -f values.yaml
   ```

1. Wait for the deployment to complete. You can optionally use k9s to monitor the progress:

   ```
   k9s
   ```

1. Once the deployment is complete, you can access the WikiLambda CI environment at:

   ```
   http://<your-private-ip>.sslip.io
   ```

### Uninstalling the Chart

1. To uninstall the WikiLambda CI environment, run:

   ```
   helm uninstall wikilambda
   ```

Please refer to the [Helm documentation](https://helm.sh/docs/) for more information on customizing your deployment and managing the release.
