"""This script generates a configuration string for the orchestrator.

The service names provided here correspond to the service names given in
templates/*-function-evaluator.yaml.

Usage:
$ python generate_environment_variables.py
"""


import json


def _evaluator_uri_for(evaluator_service):
    return f"http://{evaluator_service}:6927/1/v1/evaluate/"


def _javascript_all_config():
    """Generate the configuration for the javascript-all evaluator.

    This service supports the provided programming languages. The name of the
    service given here must be identical to the name provided in
    templates/javascript-all-function-evaluator.
    """
    return {
        "programmingLanguages": [
                'javascript-es2020', 'javascript-es2019', 'javascript-es2018',
                'javascript-es2017', 'javascript-es2016', 'javascript-es2015',
                'javascript'
        ],
        "evaluatorUri": _evaluator_uri_for(
                "{{ .Release.Name }}-javascript-all-function-evaluator"),
        "evaluatorWs": "",
        "useReentrance": False,
    }


def _python_all_config():
    """Generate the configuration for the python-all evaluator.

    This service supports the provided programming languages. The name of the
    service given here must be identical to the name provided in
    templates/python-all-function-evaluator.
    """
    return {
        "programmingLanguages": [
                'python-3-9', 'python-3-8', 'python-3-7', 'python-3', 'python'
        ],
        "evaluatorUri": _evaluator_uri_for(
                "{{ .Release.Name }}-python3-all-function-evaluator"),
        "evaluatorWs": "",
        "useReentrance": False,
    }


def generate_orchestrator_config_string():
    configuration = {
        "evaluatorConfigs": [
            _python_all_config(),
            _javascript_all_config(),
        ]
    }
    return json.dumps(configuration)


if __name__ == '__main__':
    print(generate_orchestrator_config_string())
